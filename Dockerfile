# Previously based on https://github.com/just-containers/s6-overlay
# Moved to http://phusion.github.io/baseimage-docker/ because I feel
# more comfortable with runit than S6
FROM phusion/baseimage

# Heavily borrowed from
# https://github.com/eugeneware/docker-wordpress-nginx/blob/master/Dockerfile
MAINTAINER Rafael Rivero <private@email.com>

# Ubuntu package version
ENV LSB_RELEASE=trusty

# MongoDB version
ENV MONGO_VERSION=3.0

# NVM version
ENV NVM_VERSION=0.29.0

# Let the container know that there is no tty
ENV DEBIAN_FRONTEND noninteractive

# Install mongodb repository
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10 && \
    echo "deb http://repo.mongodb.org/apt/ubuntu ${LSB_RELEASE}/mongodb-org/${MONGO_VERSION} multiverse" > /etc/apt/sources.list.d/mongodb-org-${MONGO_VERSION}.list

# Install base APT packages
RUN apt-get update && \
    apt-get -y install vim less curl unzip net-tools \
    git build-essential libssl-dev python python-setuptools tmux \
    mysql-client nginx \
    mongodb-org-shell mongodb-org-tools && \
    apt-get clean

# Copy skel files to /opt, and add node modules to path
RUN cp /etc/skel/.bash_logout /opt && \
    cp /etc/skel/.bashrc      /opt && \
    cp /etc/skel/.profile     /opt

# Change /opt ownership to www-data
RUN usermod -s /bin/bash -d /opt www-data && \
    chown -R www-data:www-data /opt

# Allow www-data user to run sudo commands
ADD files/sudoers.d/www-data /etc/sudoers.d/www-data
RUN chmod 0440 /etc/sudoers.d/www-data

# Change to user www-data for SDKs
USER www-data

# Install cloud9 web editor
RUN git clone git://github.com/c9/core.git /opt/c9sdk && \
    cd /opt/c9sdk && \
    scripts/install-sdk.sh

# Install nvm
RUN curl -o- https://raw.githubusercontent.com/creationix/nvm/v${NVM_VERSION}/install.sh | bash

# Node version required by Strongloop ARC
# Strongloop strong-pm is not yet compatible with stable (node 4.1) 
# or iojs (3.3) 
ENV ARC_NODE_VERSION=iojs

# Install node
RUN export NVM_DIR=/opt/.nvm        && \
    . ${NVM_DIR}/nvm.sh             && \
    nvm install ${ARC_NODE_VERSION} && \
    echo ${ARC_NODE_VERSION} > /opt/.nvmrc

# Install strongloop
RUN export NVM_DIR=/opt/.nvm    && \
    . ${NVM_DIR}/nvm.sh         && \
    nvm use ${ARC_NODE_VERSION} && \
    cd /opt                     && \
    npm install strongloop      && \
    echo 'export PATH="$PATH:/opt/node_modules/.bin"' >> /opt/.bashrc

# Install typescript development libs
RUN export NVM_DIR=/opt/.nvm    && \
    . ${NVM_DIR}/nvm.sh         && \
    nvm use ${ARC_NODE_VERSION} && \
    cd /opt                     && \
    npm install typescript      && \
    npm install tsd

# Back to user root
USER root

# Configure nginx
RUN echo "daemon off;" >> /etc/nginx/nginx.conf

# Add nginx site
ADD files/nginx/default.keep.conf /etc/nginx/sites-available/default.keep.conf
ADD files/nginx/default.new.conf  /etc/nginx/sites-available/default.new.conf

# C9 user default password
ENV C9_PASSWORD=password

# Loopback application name
ENV APP_NAME "app"

# What to do with the nginx proxy headers:
# - "keep" to keep the headers receiver from an external proxy
# - "new"  to generate new proxy headers
ENV PROXY_HEADERS "keep"

# 8080: NGINX Port
# 8081: Cloud9 port, since URL prefix does not work...
# 8082: Strongloop ARC Port
# 3001: Puerto debug de node
# 5858: Websocket de debug de node
EXPOSE 8080 8081 8082 3001 5858

# Volumes for development pages and apps
VOLUME ["/opt/app", "/opt/www"]

# Add runit configuration
ADD files/service/nginx  /etc/service/nginx
ADD files/service/c9     /etc/service/c9
ADD files/service/slc    /etc/service/slc
ADD files/my_init.d      /etc/my_init.d

# Save variables in run scripts
RUN sed -i "s/\$APP_NAME/$APP_NAME/g"                 /etc/my_init.d/run   &&\
    sed -i "s/\$PROXY_HEADERS/$PROXY_HEADERS/g"       /etc/my_init.d/run   &&\
    sed -i "s/\$ARC_NODE_VERSION/$ARC_NODE_VERSION/g" /etc/my_init.d/run   &&\
    sed -i "s/\$C9_PASSWORD/$C9_PASSWORD/g"           /etc/service/c9/run &&\
    sed -i "s/\$ARC_NODE_VERSION/$ARC_NODE_VERSION/g" /etc/service/slc/run
