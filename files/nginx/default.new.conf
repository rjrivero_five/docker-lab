##
# You should look at the following URL's in order to grasp a solid understanding
# of Nginx configuration files in order to fully unleash the power of Nginx.
# http://wiki.nginx.org/Pitfalls
# http://wiki.nginx.org/QuickStart
# http://wiki.nginx.org/Configuration
#
# Generally, you will want to move this file somewhere, and start with a clean
# file but keep this around for reference. Or just disable in sites-enabled.
#
# Please see /usr/share/doc/nginx-doc/examples/ for more detailed examples.
##

server {

    root        /opt/www;
    server_name localhost;
    listen      8080;
    index       index.html index.htm;

    # Disable sendfile as per
    # https://docs.vagrantup.com/v2/synced-folders/virtualbox.html
    sendfile off;

#    #  static content
#    location ~ \.(?:ico|jpe?g|jpeg|gif|css|png|js|swf|xml|woff|eot|svg|ttf|html)$ {
#        access_log  off;
#        add_header  Pragma public;
#        add_header  Cache-Control public;
#        expires     30d;  
#    }

    # proxy requests to node
    location @proxy {
        proxy_http_version 1.1;
        proxy_set_header   HOST $http_host;
        proxy_set_header   X-Forwarded-Proto $scheme;
        proxy_set_header   X-Real-IP $remote_addr;
        proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header   X-NginX-Proxy true;

        proxy_pass http://localhost:3000;
        proxy_redirect off;
        break;
    }

    location / {
        try_files $uri $uri/ @proxy;
    }

    # redirect server error pages to the static page /50x.html
    #
    error_page 500 502 503 504 /50x.html;
    location = /50x.html {
        root /usr/share/nginx/www;
    }

    # deny access to .htaccess files, if Apache's document root
    # concurs with nginx's one
    #
    location ~ /\.ht {
        deny all;
    }

}

